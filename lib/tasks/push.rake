namespace :push_line do 
  desc "push_line"
  task schedule_alert: :environment do
    puts "cron起動"
    message = {
      type: 'template',
      altText: '',
      template: {
        type: 'buttons',
        text: '',
        actions: [
          {
            type: 'message',
            label: '服用できました',
            text:  ''
          },
          {
            type: 'message',
            label: '服用できませんでした',
            text: ''
          }
        ]
      }
    }
    client = Line::Bot::Client.new { |config|
#LINE_CHANNEL_SECRET=90170b40f660975393304cf856c4eddc
#LINE_CHANNEL_TOKEN=6xYkp3TYaK0s0cffLwXzhYb+6YWhY0OIsS5qgayeFHUv6TAo7KhjKO7ImO4OrqUlX87mQR3eAsQ8/ITGnPsg+QEd25JeIontw+quWb2gdlIih38uMv/Bup2mGkFvd9QL0+n+1n4cvR2aoH3knpp4GQdB04t89/1O/w1cDnyilFU=
      config.channel_secret = "cffb31e27f728b18353d6afb16772d3d"
      config.channel_token = "Zfu1P9Zih7NddPmeqNy/XZ/YWM5NWFomiWhSP/hYl3wGDQ8EXEGo8RPB8bvm+IxB/od3SWWdurqWDQC8+IIqWvyVtv/YllOOPZ8/UMOg4NsPCkCQa0/h9NNQlrzvsmhYXQyp/WfRuRJEhD/2s9SrwAdB04t89/1O/w1cDnyilFU="
    }
    puts  ENV["LINE_CHANNEL_SECRET"] 
    #Schedule.where(user_id: 0).each do |rec|
    Schedule.where(taking_hour: Time.now.in_time_zone.hour, taking_minute: Time.now.in_time_zone.min).each do |rec|
    #Schedule.where(id: 1133).each do |rec|
      
      user = User.find_by(line_user_id: rec.line_user_id)
      user_day = (Time.now.in_time_zone.to_date - user.created_at.in_time_zone.to_date).to_i + 1
      bot_message_key = rec.is_lotriga? ? 16 : 25
      user_yes_key = rec.is_lotriga? ? 13 : 15
      user_no_key = rec.is_lotriga? ? 14 : 16
      next if user_day > 90
      next if MedicationLog.where(user_id: rec.line_user_id, day: user_day, seq: rec.taking_num, is_lotrigger: rec.is_lotriga).count > 0
      puts MedicationLog.where(user_id: rec.line_user_id, day: user_day, seq: rec.taking_num, is_lotrigger: rec.is_lotriga).to_sql
      #binding.pry
      message[:altText] = rec.is_lotriga == 1 ? "ロトリガを食後すぐに服用しましょう" : "お薬を服用する時間です"
      #puts logm.to_h
      message[:template][:text] = Time.now.in_time_zone.strftime("【%-m月%-d日 %-H時%-M分】\n") + BotMessage.find_by(id: bot_message_key).text
      message[:template][:actions][0][:text] = UserMessage.find_by(id: user_yes_key).text + Time.now.in_time_zone.strftime("\n(%Y-%m-%d %H:%M:0") + rec.taking_num.to_s + ")"
      message[:template][:actions][1][:text] = UserMessage.find_by(id: user_no_key).text
#      puts message
      response = client.push_message(rec.line_user_id, message)
      puts response.code
      #puts MedicationLog.create(user_id: rec.line_user_id, day: user_day, seq: rec.taking_num, is_lotrigger: rec.is_lotriga)
      MedicationLog.create(user_id: rec.line_user_id, day: user_day, seq: rec.taking_num, is_lotrigger: rec.is_lotriga) if response.code == "200"
    end
    #p response
  end
end
