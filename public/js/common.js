$(document).ready(function() {
    var initialLocaleCode = 'ja';

    $('#calendar').fullCalendar({

        themeSystem: 'bootstrap4',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month'
        },
        defaultDate: new Date(),
        locale: initialLocaleCode,
        buttonIcons: false, // show the prev/next text
        weekNumbers: false,
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: false, // allow "more" link when too many events
        eventSources: [{
            url: '/api/v1/medication_logs?url_param=' + $('#calendar').data("hex"),
            dataType: 'json',
            async: false,
            type: 'GET',
            error: function () {
            }
        }],
        //ブラウザのWindowsサイズ取得してリサイズし直す
        height: window.innerHeight - 300, // ①
        windowResize: function () { // ②
            $('#calendar').fullCalendar('option', 'height', window.innerHeight - 300);
        }
    });
});
