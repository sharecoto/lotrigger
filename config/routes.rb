Rails.application.routes.draw do
  post '/callback' => 'linebot#callback'

  namespace 'api' do
    namespace 'v1' do
      get "/term" => "term#show"
      #get "/calendar" => "calendar#index"
      #get "/calendar/" =>
      post "/commit" => "term#commit"
      resources :calendar, :only => [:index, :show, :edit], param: :line_user_id
      resources :users
      resources :medication_logs, :only => :index
    end
  end
end
