module Api
  module V1
    class UsersController < ActionController::Base
      before_action :set_user, only: [:show, :update, :destroy]

      def index
        @users = User.order(created_at: :desc)
        render "users/index"
        #bot_message = BotMessage.find(1)
        #user_message = UserMessage.find(1)
        #render json: { status: 'SUCCESS', message: 'Loaded users', data: [users, bot_message, user_message] }
      end

      def show
        render json: { status: 'SUCCESS', message: 'Loaded the user', data: @user }
      end

      def create
        user = User.new(user_params)
        if user.save
          render json: { status: 'SUCCESS', data: user }
        else
          render json: { status: 'ERROR', data: user.errors }
        end
      end

      def destroy
        @user.destroy
        render json: { status: 'SUCCESS', message: 'Deleted the user', data: @user }
      end

      def update
        if @user.update(user_params)
          render json: { status: 'SUCCESS', message: 'Updated the user', data: @user }
        else
          render json: { status: 'SUCCESS', message: 'Not updated', data: @user.errors }
        end
      end

      private

      def set_user
        @user = User.find(params[:id])
      end

      def user_params
        params.require(:user).permit(:title)
      end
    end
  end
end
