module Api
  module V1
    class TermController < ActionController::Base
      require 'line/bot'           
      def client                   
#    binding.pry               
        @client ||= Line::Bot::Client.new { |config|
          config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
          config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
        }
      end
      before_action :set_user, only: [:show, :commit]

      def show
        render "users/index"
        #bot_message = BotMessage.find(1)
        #user_message = UserMessage.find(1)
        #render json: { status: 'SUCCESS', message: 'Loaded users', data: [users, bot_message, user_message] }
      end

      def commit
        response = client.push_message(@user.line_user_id, template_debug)
        @user.state = 1
        @user.save!
        render "term/commit" 
        #binding.pry
      end

      private

      def set_user
        @user = User.find_by(url_param: params[:url_param])
      end

      def term_params
        params.require(:term).permit(:url_param)
      end

      def template_debug
        {
          "type": "template",
          "altText": BotMessage.find(5).text,
          "template": {
            "type": "buttons",
            "text": BotMessage.find(5).text,
            "actions": [
              {
                "type": "message",
                "label": "１回",
                "text": UserMessage.find(5).text,
              },
              {
                "type": "message",
                "label": "２回",
                "text": UserMessage.find(6).text,
              }
            ]
          }
        }
      end
    end
  end
end

