module Api
  module V1
    class MedicationLogsController < ActionController::Base
      before_action :set_user, only: [:index]

      def index
        colors = ['rgb(89,89,89)', 'rgb(90,177,82)']
        start_time = params[:start].in_time_zone.in_time_zone("UTC").strftime("%Y-%m-%d %H:%M:%S") || Time.now.in_time_zone.beginning_of_month
        end_time   = params[:end].in_time_zone.end_of_day.in_time_zone("UTC").strftime("%Y-%m-%d %H:%M:%S") || Time.now.in_time_zone.end_of_month
        alert_logs = MedicationLog.where(
          user_id: @user.line_user_id,
          status: 1,
          created_at: start_time..end_time
        ).order(:created_at).map { |log|
          hash = {
            'title': '●',
            'start': log.created_at.in_time_zone.strftime('%Y-%m-%d %H:%M:%S'),
            'color': 'rgb(255,255,255)',
            'textColor': colors[log.is_lotrigger]
          }
        }
        #head :ok
        #binding.pry
        render json: alert_logs
        #bot_message = BotMessage.find(1)
        #user_message = UserMessage.find(1)
        #render json: { status: 'SUCCESS', message: 'Loaded users', data: [users, bot_message, user_message] }
      end

      private

      def set_user
        @user = User.find_by(url_param: params[:url_param])
          #binding.pry
      end

      def user_params
      end
    end
  end
end
