module Api
  module V1
    class CalendarController < ActionController::Base
      require 'line/bot'           
      def client
#    binding.pry               
        @client ||= Line::Bot::Client.new { |config|
          config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
          config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
        }
      end
      before_action :set_user, only: [:show, :edit]

      def index
        
        render "calendar/index"
        #bot_message = BotMessage.find(1)
        #user_message = UserMessage.find(1)
        #render json: { status: 'SUCCESS', message: 'Loaded users', data: [users, bot_message, user_message] }
      end

      def show
        @times = Array.new(5, "")
        #binding.pry
        Schedule.where(line_user_id: @user.line_user_id).order(:is_lotriga, :taking_num).each { |schedule|
          @times[schedule.is_lotriga * 3 + schedule.taking_num - 1] = format("%02d:%02d", schedule.taking_hour.to_s, schedule.taking_minute.to_s)
          #binding.pry
        }
        
        render "calendar/edit"
      end

      def edit
        params[:times].each_with_index { |time, idx|
          is_lotriga = idx < 2 ? 1 : 0
          taking_num = idx + 1
          taking_num -= 2 if is_lotriga == 0
          if time.blank?
            Schedule.find_or_create_by(line_user_id: @user.line_user_id, is_lotriga: is_lotriga, taking_num: taking_num).delete
          else
            hour = time.split(":")[0] || 0
            min = time.split(":")[1] || 0
            Schedule.find_or_create_by(line_user_id: @user.line_user_id, is_lotriga: is_lotriga, taking_num: taking_num).update(taking_hour: hour, taking_minute: min)
          end
        }
        @times = Array.new(5, "")
        Schedule.where(line_user_id: @user.line_user_id).order(:is_lotriga, :taking_num).each { |schedule|
          @times[schedule.is_lotriga * 3 + schedule.taking_num - 1] = format("%02d:%02d", schedule.taking_hour.to_s, schedule.taking_minute.to_s)
          #binding.pry
        }
        puts @times        
        render "calendar/edit"
        #Schedule.where(line_user_id: @user.line_user_id). 
      end

      private

      def calendar_params
        params.require(:calendar).permit(:url_param)
      end

      def set_user
        @user = User.find_by(line_user_id: params[:line_user_id])
      end

    end
  end
end

