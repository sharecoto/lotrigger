class LinebotController < ApplicationController
  require 'line/bot'
  def client
#    binding.pry
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def callback
    body = request.body.read
    bot_message = YAML.load_file(Rails.root.join('config', 'bot_message_ja.yml'))
    yml = YAML.load_file(Rails.root.join('config', 'bot_message_ja.yml'))

    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature)
      head :bad_request
    end
    events = client.parse_events_from(body)
    #binding.pry
    events.each { |event|
      user_id = event["source"]["userId"]
      case event
      when Line::Bot::Event::Message
        case event.type
        when Line::Bot::Event::MessageType::Text
          
#          if event.message['text'].eql?("デバッグ")
#            yml_key = "follow"
#            response = client.get_profile(user_id)
#            user_greet = "こんにちは、#{JSON.parse(response.body)['displayName']}さん。"
#            template = create_template(yml, yml_key)
#            template[:template][:text] = template[:altText] = user_greet + template[:altText]
#            exec(yml_key, event)
#            response = client.reply_message(event['replyToken'], template)
#            puts template
#            puts response.code
#          end
          text = event.message['text'].sub(/\n\([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\)/, "")
          yml_key = UserMessage.find_by(text: text)&.id&.to_s
          yml_key = exec(yml_key, event)
          yml_key = "other"  unless yml_key
          return  if yml_key == "exit"
          template = ""
          if yml_key.class == Array then
            template = yml_key.map { |key| create_template(yml, key)}
          else
            template = create_template(yml, yml_key)
          end
          template[:template][:actions][0][:uri] += User.find_by(line_user_id: user_id).url_param if yml_key == "1" || yml_key == "11"
          template[:template][:actions][0][:uri] += User.find_by(line_user_id: user_id).line_user_id  if yml_key == "12"
          if yml_key == "19"
            user = User.find_by(line_user_id: user_id)
            user_day = (Time.now.in_time_zone.to_date - user.created_at.in_time_zone.to_date).to_i + 1
            memo =  WordMemo.find_by(send_day_count: user_day)&.text || "今日のコラムはありません"
            template[:text] += "今日は" + user_day.to_s + "日目。" + memo
          end
          response = client.reply_message(event['replyToken'], template)
#          puts template
        end 
      when Line::Bot::Event::Follow
        yml_key = "follow"
        return unless yml[yml_key]
        response = client.get_profile(user_id)
        user_greet = "こんにちは、#{JSON.parse(response.body)['displayName']}さん。"
        template = create_template(yml, yml_key)
        template[:template][:text] = template[:altText] = user_greet + template[:altText]
        yml_key = exec(yml_key, event)
        client.reply_message(event['replyToken'], template)
      when Line::Bot::Event::Unfollow
        user_id = event["source"]["userId"]
        User.find_by(line_user_id: user_id).delete
        Schedule.where(line_user_id: user_id).delete_all
        MedicationLog.where(user_id: user_id).delete_all
      when Line::Bot::Event::Postback
        yml_key = event["postback"]["data"]
        yml_key = exec(yml_key, event)
        template = create_template(yml, yml_key)
        puts template
        response = client.reply_message(event['replyToken'], template)
      end
    }

    head :ok
  end

  private
  def sym_keys(source)
    if source.class == Array
      return source.map(&:deep_symbolize_keys)
    else
      return source&.deep_symbolize_keys
    end    
  end

  def create_template(yml, key)
    
    template = sym_keys(yml[key])
    return decorate_item(yml, key) if template.nil?

    if template.class == Array
      templates =  template.map { |item| decorate_item item, key }
    else
      return decorate_item(yml, key) if template.nil?
      template = decorate_item(template, key)
      return template
    end 
  end

  def decorate_item(template, key)
    if key.include?("word") then
      template = sym_keys(template["word"])
      word_id = key.delete("word").to_i
      memo = WordMemo.find_by(send_day_count: word_id)
      template[:altText] = template[:template][:text] = memo["text"]
      template[:template][:thumbnailImageUrl] = ENV["DOMAIN_NAME"] + memo["image_url"]
      template[:template][:actions][0][:uri] = ENV["DOMAIN_NAME"] + memo["url"]
      return template
   else
      case template[:type]
      when "template"
        template[:altText] = BotMessage.find(template[:altText])&.text if template[:altText]&.to_i > 0
        template[:template][:text] = BotMessage.find(template[:template][:text]).text if template[:template][:text]&.to_i > 0
        template[:template][:thumbnailImageUrl] = ENV["DOMAIN_NAME"] + template[:template][:thumbnailImageUrl] if template[:template][:thumbnailImageUrl].present?
        
        template[:template][:actions].map { |action|
          action[:uri] = ENV["DOMAIN_NAME"] + action[:uri] if action[:uri].present?
          next if action[:text].blank? || action[:text].zero?
          action[:text] = UserMessage.find(action[:text]).text
        }
      when "text"
        template[:text] = BotMessage.find(template[:text]).text if template[:text]&.to_i > 0
      end
    end
    template
  end

  def exec(key, event)
    user_id = event["source"]["userId"]
    case key
    when "1", "follow"
      User.find_or_create_by(line_user_id: user_id) do |user|
        user.url_param = SecureRandom.alphanumeric(32)
      end
    when "3"
      user = User.find_by(line_user_id: user_id).update(status: 1).update(state: 1)
    when "5", "6"
      user = User.find_by(line_user_id: user_id)
      user.lotrigger_count = 1
      user.lotrigger_count = 2 if key == "6"
      user.save!
      Schedule.where(user_id: user_id, is_lotriga: 1)
    when "9", "10", "17" 
      user = User.find_by(line_user_id: user_id)
      user.other_count = 1
      user.other_count = 2 if key == "10"
      user.other_count = 3 if key == "17"
      user.save!
      Schedule.where(line_user_id: user_id, is_lotriga: 0)
    when "t1","t2","o1","o2","o3"
      user = User.find_by(line_user_id: user_id)
      is_lotriga = key.include?("t") ? 1 : 0
      seq = key[1].to_i
      times = event["postback"]["params"]["time"].split(":")
      schedule = Schedule.find_or_initialize_by(user_id: 0, line_user_id: user_id, is_lotriga: is_lotriga, taking_num: seq)
      schedule.update(taking_hour: times[0], taking_minute: times[1])
      key = "ct" if is_lotriga == 1 && user.lotrigger_count <= seq
      key = "8" if is_lotriga == 0 && user.other_count <= seq
    when "13", "15", "14", "16"
      seq = event.message['text'].count(" ")
      is_lotriga = key == "13" ? 1 : 0
      user = User.find_by(line_user_id: user_id)
      user_day = (Time.now.in_time_zone.to_date - user.created_at.in_time_zone.to_date).to_i + 1
      count = MedicationLog.where(user_id: user.line_user_id, day: user_day, seq: 0).count
      if count == 0 then
        MedicationLog.create(user_id: user.line_user_id, day: user_day, seq: 0).save!
        word = WordMemo.find_by(send_day_count: user_day)&.text
      end
      if key == "13" || key == "15" then
        if event.message['text'].match(/([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})/) then
           logtime = event.message['text'].match(/([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})/)[1].to_time
           user_day2 = (logtime.to_date - user.created_at.in_time_zone.to_date).to_i + 1
           click_alert = MedicationLog.where(user_id: user.line_user_id, day: user_day2, seq: logtime.sec, is_lotrigger: is_lotriga).first
           click_alert&.update(status: 1)
           key = word.present? ? ["word" + user_day.to_s] : ["exit"]
        else
          click_alert = MedicationLog.where(user_id: user.line_user_id, day: user_day, seq: seq, is_lotrigger: is_lotriga).first
          click_alert&.update(status: 1)
          key = word.present? ? ["word" + user_day.to_s] : ["exit"]
        end
      else
        key = [key, "word" + user_day.to_s] if word.present?
      end
      key.push "final" if user_day == 90
    when "19"
      user = User.find_by(line_user_id: user_id)
      user.created_at -= 1.days
      user.save!
      schedule = Schedule.find_by(line_user_id: user.line_user_id, is_lotriga: 1, taking_num: 1)
      time = Time.now.in_time_zone + 1.minute
      schedule.taking_hour = time.hour
      schedule.taking_minute = time.min
      schedule.save!
    end
    return key
  end
end
