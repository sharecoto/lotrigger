<?php
// HTTPヘッダを設定

require_once ('/var/www/html/line-medical-alarm/lotriga/functions/Schedule.php');
require_once ('/var/www/html/line-medical-alarm/lotriga/functions/Result.php');
require_once ('/var/www/html/line-medical-alarm/lotriga/functions/User.php');
require_once('/var/www/html/line-medical-alarm/lotriga/functions/BotMessage.php');
require_once('/var/www/html/line-medical-alarm/lotriga/functions/UserMessage.php');

$channelToken = 'uKTtA/zeejxv7NQNFs/mIp5WlnM/imQvlLv2QTZ5pRzZcGet6oSXJW6W3IzHRhXrlhD3FkoQOui4mpzinIqZ+OYPOlmO877+xoKGuuCuZC+gGELD4nlGu+rrs7EpBOdY7YllMNK0cHMJFpbJ3aGdAAdB04t89/1O/w1cDnyilFU=';
$headers = [
    'Authorization: Bearer ' . $channelToken,
    'Content-Type: application/json; charset=utf-8',
];

$schedule = new Schedule();
$pushLists = $schedule->getPushLists();
$user = new User();

$botMessage = new BotMessage();
$userMessage = new UserMessage();

foreach ($pushLists as $pushList){

    $result = new Result();
    $scheduleId = $pushList["id"];
    $isLotriga = $pushList["is_lotriga"];
    $takingHour = $pushList["taking_hour"];
    $takingMinute = $pushList["taking_minute"];
    $resultId = $result->insert($scheduleId, $takingHour, $takingMinute);

    if ($user->getDayCountFromCreated($pushLists['line_user_id']) == 90){
        if ($pushList['id'] == $schedule->getLastAlarmOfDay($pushList['line_user_id'])){
            $wordMemoParam = '&wordMemo=true';
        }else{
            $wordMemoParam = '';
        }
    }else{
        if ($pushList['id'] == $schedule->getFirstAlarmOfDay($pushList['line_user_id'])){
            $wordMemoParam = '&wordMemo=true';
        }else{
            $wordMemoParam = '';
        }
    }

    if($isLotriga == 1){
        $bMessage = $botMessage->getMessage(16);
        $uMessageYes = $userMessage->getMessage(13);
        $uMessageNo = $userMessage->getMessage(14);
    }else{
        $bMessage = $botMessage->getMessage(25);
        $uMessageYes = $userMessage->getMessage(15);
        $uMessageNo = $userMessage->getMessage(16);
    }

    $post = [
        'to' => $pushList['line_user_id'],
//        'to' => 'Ubc31116f99289fef3f127fa9ec33b1c4',
        'messages' => [
            [
                'type' => 'template',
//                'altText' => 'ロトリガを食後すぐに服用しましょう',
                'altText' => 'お薬を服用する時間です',
                'template' => array(
                    'type' => 'buttons',
                    'text' => '【' .
                        sprintf('%01d', date('m')). '月'.
                        sprintf('%01d', date('d')).'日'.
                        sprintf('%01d', $pushList["taking_hour"]).'時'.
                        sprintf('%01d', $pushList["taking_minute"]).'分'.
                        "】\n".$bMessage,
                    'actions' => [
                        array(
                            'type' => 'postback',
                            'label' => '服用できました',
                            'displayText' => $uMessageYes,
                            'data' => 'resultId='.$resultId.'&status=success'. $wordMemoParam,
                        ),
                        array(
                            'type' => 'postback',
                            'label' => '服用できませんでした',
                            'displayText' => $uMessageNo,
                            'data' => 'resultId='.$resultId.'&status=failed'. $wordMemoParam,
                        )
                    ]
                ),
            ],
        ],
    ];

    $post = json_encode($post);
    $ch = curl_init('https://api.line.me/v2/bot/message/push');

    $options = [
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_BINARYTRANSFER => true,
        CURLOPT_HEADER => true,
        CURLOPT_POSTFIELDS => $post,
    ];
    curl_setopt_array($ch, $options);

// 実行
    $pushResult = curl_exec($ch);
    curl_close($ch);
}

// Webhook URL
//$url = "https://hooks.slack.com/services/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";
//
//// メッセージ
//
//$nowTime =  str_split(str_replace(':','', date('H:i')),2);
//$nowHour = sprintf('%02d', $nowTime[0]);
//$nowMinute = sprintf('%02d', $nowTime[1]);
//
//$ids = array();
//foreach ($pushLists as $pushList){
//    array_push($ids,$pushList['line_user_id']);
//}
//
//$ids = implode(" / ", $ids);
//if(!count($pushLists)){
//    $ids = "該当なし";
//}
//$message = array(
//    "username"   => "",
//    "icon_emoji" => ":slack:",
//    "text"       => $nowHour."時".$nowMinute."分 送信数:".count($pushLists)."件 UserID:".$ids
//);
//
//// メッセージをjson化
//$message_json = json_encode($message);
//
//// payloadの値としてURLエンコード
//$message_post = "payload=".urlencode($message_json);
//
//$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, $url);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $message_post);
//curl_exec($ch);
//curl_close($ch);