class AddDaycountToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :day_count, :integer, default: 1, after: :lotrigger_count
  end
end
