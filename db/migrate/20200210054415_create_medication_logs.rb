class CreateMedicationLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :medication_logs do |t|
      t.integer :user_id
      t.integer :day, default: 1
      t.integer :is_lotrigger, default: 1
      t.integer :seq, default: 1
      t.timestamps
    end
  end
end
