class AddLineUserIdToSchedule < ActiveRecord::Migration[5.1]
  def change
        add_column :schedules, :line_user_id, :string, default: "", after: :user_id
  end
end
