class AddUserparamToMedicationlog < ActiveRecord::Migration[5.1]
  def change
    add_column :medication_logs, :user_param, :string, default: "", after: :user_id
  end
end
