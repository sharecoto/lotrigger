class AddColumnsToMedicationlogs < ActiveRecord::Migration[5.1]
  def change
    add_column :medication_logs, :status, :integer, default: 0, after: :day
  end
end
