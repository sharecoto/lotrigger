class RenameUserparamToMedicationlog < ActiveRecord::Migration[5.1]
  def change
    rename_column :medication_logs, :user_param, :url_param
  end
end
