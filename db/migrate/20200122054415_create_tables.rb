class CreateTables < ActiveRecord::Migration[5.1]
  def change
    create_table "bot_messages", id: :integer, comment: "ID", default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
        t.string "text", null: false, comment: "本文"
      t.datetime "created_at", null: false, comment: "作成日"
    t.datetime "updated_at", null: false, comment: "更新日"
    end

    create_table "results", id: :integer, comment: "ID", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.string "schedule_id", limit: 45, null: false, comment: "スケジュールID"
      t.string "status", limit: 45, comment: "服薬結果(成功:success,失敗:failed)"
      t.datetime "took_time", null: false, comment: "服薬した時間"
      t.datetime "created_at", null: false, comment: "作成日"
      t.datetime "updated_at", null: false, comment: "更新日"
    end

    create_table "schedules", id: :integer, comment: "ID", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.integer "user_id", null: false, comment: "ユーザID"
      t.integer "is_lotriga", null: false, comment: "服薬種類(NULL:未入力,0:その他,1:ロトリガ)"
      t.integer "taking_num", null: false, comment: "服薬回数(何度目か)"
      t.integer "taking_hour", null: false, comment: "服薬予定(時)"
      t.integer "taking_minute", null: false, comment: "服薬予定(分)"
      t.datetime "created_at", null: false, comment: "作成日時"
      t.datetime "updated_at", null: false, comment: "更新日時"
      t.index ["user_id"], name: "index_schedules_on_user_id"
    end

    create_table "user_messages", id: :integer, comment: "ID", default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.string "text", null: false, comment: "本文"
      t.datetime "created_at", null: false, comment: "作成日"
      t.datetime "updated_at", null: false, comment: "更新日"
    end

    create_table "users", id: :integer, comment: "ID", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.string "line_user_id", limit: 45, null: false, comment: "LINEユーザID"
      t.string "url_param", limit: 64, null: false, comment: "カレンダーURL用の引数"
      t.datetime "created_at", null: false, comment: "作成日時"
      t.datetime "updated_at", null: false, comment: "更新日時"
      t.datetime "deleted_at"
    end

    create_table "word_memo_send_histories", primary_key: "ID", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.integer "word_memo_id", null: false
      t.integer "user_id", null: false, comment: "ユーザID"
      t.datetime "created_at", null: false, comment: "作成日"
      t.datetime "updated_at", null: false, comment: "更新日"
      t.index ["user_id"], name: "index_word_memo_send_histories_on_user_id"
      t.index ["word_memo_id"], name: "index_word_memo_send_histories_on_word_memos_id"
    end

    create_table "word_memos", id: :integer, comment: "ID", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
      t.string "send_day_count", limit: 45, null: false
      t.string "text", limit: 1000, null: false, comment: "本文"
      t.string "image_url", limit: 1000, null: false
      t.string "url", limit: 1000, null: false, comment: "URL"
      t.string "send_schedule", limit: 45, null: false
      t.datetime "created_at", null: false, comment: "作成日時"
      t.datetime "updated_at", null: false, comment: "更新日時"
      t.index ["id"], name: "id_UNIQUE", unique: true
    end

  end
end
