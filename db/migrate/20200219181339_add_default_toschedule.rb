class AddDefaultToschedule < ActiveRecord::Migration[5.1]
  def change
    change_column :schedules, :user_id, :integer, default: 0
    change_column :schedules, :taking_hour, :integer, default: 0
    change_column :schedules, :taking_minute, :integer, default: 0
  end
end
