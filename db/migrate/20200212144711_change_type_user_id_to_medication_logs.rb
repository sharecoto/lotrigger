class ChangeTypeUserIdToMedicationLogs < ActiveRecord::Migration[5.1]
  def change
    change_column :medication_logs, :user_id, :text
  end
end
