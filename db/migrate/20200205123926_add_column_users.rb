class AddColumnUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :lotrigger_count, :integer, default: 0, after: :url_param
    add_column :users, :other_count, :integer, default: 0, after: :url_param
    add_column :users, :state, :integer, after: :url_param
  end
end
